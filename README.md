# Contact APP

## Client

Pour lancer le client (basé sous Angular 10) lancer la commande suivante :

``ng serve``

## Serveur

Pour lancer le serveur (basé sous NodeJS et ExpressJS) lancer la commande suivante :

``npm start``

## Dépendances

Pensez à installer les dépendances en lancant la commande suivante dans le dossier server et dans le dossier client :

``npm install``

ou si vous utilisez Yarn :

``yarn install``

## Base de données

La base contient 3 tables et utilise la technologie MySQL:

- contact
    - id
    - firstName
    - lastName
    - email
    - phoneNumber
    - createdAt
    - updatedAt

- group
    - id
    - name
    - createdAt
    - updatedAt

- contactgroup
    - contact
    - group
    - createdAt
    - updatedAt

