import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ContactService} from '../../contact/contact.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {MatDialogRef} from '@angular/material/dialog';
import {Contact} from '../../models/contact';
import {GroupService} from '../group.service';
import {Group} from '../../models/group';

@Component({
  selector: 'app-group-add',
  templateUrl: './group-add.component.html',
  styleUrls: ['./group-add.component.css']
})
export class GroupAddComponent implements OnInit {

  public groupForm = this._formBuilder.group({
    name: '',
    contacts: ''
  });
  public error = '';
  public contactsIterator: Array<string>;

  constructor(protected readonly _contactsService: ContactService,
              protected readonly _formBuilder: FormBuilder,
              protected readonly _dialogRef: MatDialogRef<GroupAddComponent>) {
  }

  ngOnInit(): void {
    this._contactsService.getContacts().subscribe(contacts => {
      this.groupForm = this._formBuilder.group({
        name: '',
        contacts: this._getTabContacts(contacts)
      });
      this.contactsIterator = Object.keys(this.groupForm.controls.contacts.value);
    });

  }

  onSubmit() {
    if (this.groupForm.value.name !== '') {
      const group: Group = {
        name: this.groupForm.value.name,
        metadatas: {
          contacts: this._getSelectedContacts(),
        }
      };
      this._dialogRef.close(
        group
      );
    } else {
      this.error = 'Le nom du groupe doit être renseigné.';
    }
  }

  private _getTabContacts(contacts: Contact[]) {
    const result = {};
    contacts.forEach(v => {
      result[`${v.firstName} ${v.lastName}`] = false;
    });
    return this._formBuilder.group(result);
  }

  private _getSelectedContacts() {
    const selectedContacts = [];
    Object.keys(this.groupForm.value.contacts).forEach(key => {
      if (this.groupForm.value.contacts[key] === true) {
        selectedContacts.push(key);
      }
    });
    return selectedContacts;
  }
}
