import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-group',
  template: '<router-outlet></router-outlet>'
})
export class GroupComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
