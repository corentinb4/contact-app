import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Group} from '../models/group';
import {Contact} from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(protected readonly _httpClient: HttpClient) {
  }

  public getGroups(): Observable<Group[]> {
    return this._httpClient.get<Group[]>('http://localhost:3000/api/group');
  }

  public addGroup(group: Group) {
    return this._httpClient.post<Group[]>('http://localhost:3000/api/group', group);
  }

  public deleteGroup(id: any) {
    return this._httpClient.delete<Group>(`http://localhost:3000/api/group/${id}`);

  }

  public updateGroup(id: number, group: Group) {
    return this._httpClient.put<Group>(`http://localhost:3000/api/group/${id}`, group);
  }
}
