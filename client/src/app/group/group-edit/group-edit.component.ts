import {Component, Inject, OnInit} from '@angular/core';
import {GroupService} from '../group.service';
import {ContactService} from '../../contact/contact.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Group} from '../../models/group';
import {Contact} from '../../models/contact';

@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.css']
})
export class GroupEditComponent implements OnInit {

  public groupForm = this._formBuilder.group({
    name: '',
    contacts: ''
  });
  public error = '';
  public contactsIterator: Array<string>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Group,
              protected readonly _contactsService: ContactService,
              protected readonly _formBuilder: FormBuilder,
              protected readonly _dialogRef: MatDialogRef<GroupEditComponent>) {
  }

  ngOnInit(): void {
    this._contactsService.getContacts().subscribe(contacts => {
      let tabContacts = this.data.metadatas.contacts.concat(contacts);
      tabContacts = tabContacts.filter((a, index, self) =>
        index === self.findIndex((b) => (
          b.id === a.id && b.id === a.id
        ))
      );
      this.groupForm = this._formBuilder.group({
        name: this.data.name,
        contacts: this._getTabContacts(tabContacts, this.data.metadatas.contacts)
      });
      this.contactsIterator = Object.keys(this.groupForm.controls.contacts.value);
    });
  }

  onSubmit() {
    if (this.groupForm.value.name !== '') {
      const group: Group = {
        name: this.groupForm.value.name,
        metadatas: {
          contacts: this._getSelectedContacts(),
        }
      };
      this._dialogRef.close(
        group
      );
    } else {
      this.error = 'Le nom du groupe doit être renseigné';
    }
  }

  private _getTabContacts(contacts: Contact[], selectedContacts: Contact[]) {
    const result = {};
    contacts.forEach(v => {
      if (selectedContacts.findIndex(selected => selected.id === v.id) > -1) {
        result[`${v.firstName} ${v.lastName}`] = true;
      } else {
        result[`${v.firstName} ${v.lastName}`] = false;
      }
    });
    return this._formBuilder.group(result);
  }

  private _getSelectedContacts() {
    const selectedContacts = [];
    Object.keys(this.groupForm.value.contacts).forEach(key => {
      if (this.groupForm.value.contacts[key] === true) {
        selectedContacts.push(key);
      }
    });
    return selectedContacts;
  }

}
