import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Group} from '../../models/group';
import {GroupService} from '../group.service';
import {ContactAddComponent} from '../../contact/contact-add/contact-add.component';
import {GroupAddComponent} from '../group-add/group-add.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {DeleteDialogComponent} from '../../utils/delete-dialog/delete-dialog.component';
import {GroupEditComponent} from '../group-edit/group-edit.component';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {
  public groups: Group[] = [];
  public displayedColumns = ['name', 'contacts', 'actions'];
  public dataSource = new MatTableDataSource(this.groups);

  constructor(protected readonly _groupService: GroupService,
              protected readonly _snackBar: MatSnackBar,
              protected readonly _matDialog: MatDialog) {
  }

  ngOnInit(): void {
    this._getData();
  }

  addGroup() {
    const dialogRef = this._matDialog.open(GroupAddComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._groupService.addGroup(result).subscribe(() => {
          this._snackBar.open('Groupe créé avec succés.', null, {
            duration: 3000,
            panelClass: ['success-snackbar']
          });
          this._getData();
        }, (err) => {
          console.error(err);
          this._snackBar.open('Erreur lors de la création du groupe.', null, {
            duration: 3000,
            panelClass: ['error-snackbar']
          });
          this._getData();
        });
      }
    });
  }

  private _getData() {
    this._groupService.getGroups().subscribe((result: Group[]) => {
      this.groups = result;
      this.dataSource = new MatTableDataSource(this.groups);
    });
  }

  editGroup(id: any) {
    const dialogRef = this._matDialog.open(GroupEditComponent, {
      data: this.groups.find(g => g.id === id)
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._groupService.updateGroup(id, result).subscribe(() => {
          this._snackBar.open('Groupe mis à jour avec succés.', null, {
            duration: 3000,
            panelClass: ['success-snackbar']
          });
          this._getData();
        }, (err) => {
          console.error(err);
          this._snackBar.open('Erreur lors de la mise à jour du groupe.', null, {
            duration: 3000,
            panelClass: ['error-snackbar']
          });
          this._getData();
        });
      }
    });
  }

  deleteGroup(id: any) {
    const dialogRef = this._matDialog.open(DeleteDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._groupService.deleteGroup(id).subscribe(() => {
          this._snackBar.open('Groupe supprimé avec succés.', null, {
            duration: 3000,
            panelClass: ['success-snackbar']
          });
          this._getData();
        }, (err) => {
          console.error(err);
          this._snackBar.open('Erreur lors de la suppression du groupe.', null, {
            duration: 3000,
            panelClass: ['error-snackbar']
          });
          this._getData();
        });
      }
    });
  }
}
