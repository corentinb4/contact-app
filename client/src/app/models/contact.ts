export class Contact {
  public id?: number = null;
  public firstName: string = null;
  public lastName: string = null;
  public email: string = null;
  public phoneNumber: string = null;
  public metadatas?: any = null;
}
