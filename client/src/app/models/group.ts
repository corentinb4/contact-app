export class Group {
  public id?: number = null;
  public name: string = null;
  public metadatas?: any = null;
}
