import {Component, OnInit} from '@angular/core';
import {ContactService} from '../contact.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialogRef} from '@angular/material/dialog';
import {Contact} from '../../models/contact';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.css']
})
export class ContactAddComponent implements OnInit {

  public contactForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl(''),
    phoneNumber: new FormControl('')
  });
  public error = '';

  constructor(protected readonly _contactsService: ContactService,
              protected readonly _snackBar: MatSnackBar,
              protected readonly _router: Router,
              protected readonly _dialogRef: MatDialogRef<ContactAddComponent>) {
  }

  ngOnInit(): void {
    this.contactForm = new FormGroup({
      firstName: new FormControl('', {validators: Validators.required}),
      lastName: new FormControl('', {validators: Validators.required}),
      email: new FormControl('', {validators: [Validators.required, Validators.email]}),
      phoneNumber: new FormControl('', {validators: [Validators.required]})
    });
  }

  onSubmit() {
    if (this.contactForm.valid) {
      const contact: Contact = {
        firstName: this.contactForm.value.firstName,
        lastName: this.contactForm.value.lastName,
        email: this.contactForm.value.email,
        phoneNumber: this.contactForm.value.phoneNumber
      };
      this._dialogRef.close(
        contact
      );
    } else {
      this.error = 'Tout les champs doivent être rensignés et l\'email doit être valide.';
    }
  }
}
