import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Contact} from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(protected readonly _httpClient: HttpClient) {
  }

  public getContacts(): Observable<Contact[]> {
    return this._httpClient.get<Contact[]>('http://localhost:3000/api/contact');
  }

  public addContact(contact: Contact): Observable<Contact> {
    return this._httpClient.post<Contact>('http://localhost:3000/api/contact', contact);
  }

  deleteContact(id: any) {
    return this._httpClient.delete<Contact>(`http://localhost:3000/api/contact/${id}`);
  }

  updateContact(id: number, contact: Contact) {
    return this._httpClient.put(`http://localhost:3000/api/contact/${id}`, contact);
  }
}
