import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Contact} from '../../models/contact';
import {ContactService} from '../contact.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  public _contactForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl(''),
    phoneNumber: new FormControl('')
  });
  public error = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: Contact,
              protected readonly _contactsService: ContactService,
              protected readonly _snackBar: MatSnackBar,
              protected readonly _dialogRef: MatDialogRef<ContactEditComponent>) {
  }

  ngOnInit(): void {
    this._contactForm = new FormGroup({
      firstName: new FormControl(this.data.firstName, {validators: [Validators.required]}),
      lastName: new FormControl(this.data.lastName, {validators: [Validators.required]}),
      email: new FormControl(this.data.email, {validators: [Validators.required, Validators.email]}),
      phoneNumber: new FormControl(this.data.phoneNumber, {validators: [Validators.required]})
    });
  }

  onSubmit() {
    if (this._contactForm.valid) {
      const contact: Contact = {
        id: this.data.id,
        firstName: this._contactForm.value.firstName,
        lastName: this._contactForm.value.lastName,
        email: this._contactForm.value.email,
        phoneNumber: this._contactForm.value.phoneNumber
      };
      this._dialogRef.close(contact);
    } else {
      this.error = 'Tout les champs doivent être renseignés et l\'email doit être valide.';
    }
  }

}
