import {Component, OnInit} from '@angular/core';
import {Contact} from '../../models/contact';
import {ContactService} from '../contact.service';
import {MatTableDataSource} from '@angular/material/table';
import {DeleteDialogComponent} from '../../utils/delete-dialog/delete-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ContactEditComponent} from '../contact-edit/contact-edit.component';
import {ContactAddComponent} from '../contact-add/contact-add.component';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  public contacts: Contact[] = [];
  public displayedColumns = ['firstName', 'lastName', 'email', 'phoneNumber', 'actions'];
  public dataSource = new MatTableDataSource(this.contacts);

  constructor(protected readonly _contactService: ContactService,
              protected readonly _snackBar: MatSnackBar,
              protected readonly _matDialog: MatDialog) {
  }

  ngOnInit(): void {
    this._getData();
  }

  addContact() {
    const dialogRef = this._matDialog.open(ContactAddComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._contactService.addContact(result).subscribe(() => {
          this._snackBar.open('Contact créé avec succés.', null, {
            duration: 3000,
            panelClass: ['success-snackbar']
          });
          this._getData();
        }, (err) => {
          console.error(err);
          this._snackBar.open('Erreur lors de la création du contact.', null, {
            duration: 3000,
            panelClass: ['error-snackbar']
          });
          this._getData();
        });
      }
    });
  }

  deleteContact(id: any) {
    const dialogRef = this._matDialog.open(DeleteDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._contactService.deleteContact(id).subscribe(() => {
          this._snackBar.open('Contact supprimé avec succés.', null, {
            duration: 3000,
            panelClass: ['success-snackbar']
          });
          this._getData();
        }, (err) => {
          console.error(err);
          this._snackBar.open('Erreur lors de la suppression du contact.', null, {
            duration: 3000,
            panelClass: ['error-snackbar']
          });
          this._getData();
        });
      }
    });
  }

  editContact(id: any) {
    const dialogRef = this._matDialog.open(ContactEditComponent, {
      data: this.contacts.find(c => c.id === id)
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._contactService.updateContact(id, result).subscribe(() => {
          this._snackBar.open('Contact édité avec succés.', null, {
            duration: 3000,
            panelClass: ['success-snackbar']
          });
          this._getData();
        }, (err) => {
          console.error(err);
          this._snackBar.open('Erreur lors de la mise à jour du contact.', null, {
            duration: 3000,
            panelClass: ['error-snackbar']
          });
          this._getData();
        });
      }
    });
  }

  private _getData() {
    this._contactService.getContacts().subscribe((result: Contact[]) => {
      this.contacts = result;
      this.dataSource = new MatTableDataSource(this.contacts);
    });
  }
}
