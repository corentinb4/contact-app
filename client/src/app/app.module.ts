import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {NotFoundComponent} from './not-found/not-found.component';
import {HomeComponent} from './home/home.component';
import {ContactListComponent} from './contact/contact-list/contact-list.component';
import {GroupListComponent} from './group/group-list/group-list.component';
import {MatTableModule} from '@angular/material/table';
import {HttpClientModule} from '@angular/common/http';
import {ContactAddComponent} from './contact/contact-add/contact-add.component';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {DeleteDialogComponent} from './utils/delete-dialog/delete-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { HeaderComponent } from './header/header.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ContactEditComponent } from './contact/contact-edit/contact-edit.component';
import { GroupAddComponent } from './group/group-add/group-add.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { GroupEditComponent } from './group/group-edit/group-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent,
    ContactListComponent,
    GroupListComponent,
    ContactAddComponent,
    DeleteDialogComponent,
    HeaderComponent,
    ContactEditComponent,
    GroupAddComponent,
    GroupEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    HttpClientModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatCheckboxModule
  ],
  entryComponents: [
    DeleteDialogComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
