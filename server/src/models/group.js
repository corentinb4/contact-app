var Sequelize = require('sequelize');
var sequelize = new Sequelize('phone_book', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

var Group = sequelize.define('group', {
    name: {
        type: Sequelize.STRING,
        field: 'name' // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    metadatas: {
        type: Sequelize.JSON,
        optional: true
    }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports = Group;
