var Sequelize = require('sequelize');
var sequelize = new Sequelize('phone_book', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

var Contact = sequelize.define('contact', {
    firstName: {
        type: Sequelize.STRING,
        field: 'firstName' // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    lastName: {
        type: Sequelize.STRING,
        field: "lastName"
    },
    email: {
        type: Sequelize.STRING,
        field: 'email' // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    phoneNumber: {
        type: Sequelize.STRING,
        field: "phoneNumber"
    }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports = Contact;
