var Sequelize = require('sequelize');
var sequelize = new Sequelize('phone_book', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

var ContactGroup = sequelize.define('contactgroup', {
    contact: {
        type: Sequelize.INTEGER,
        field: 'contact',
        primaryKey: true // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    group: {
        type: Sequelize.INTEGER,
        field: 'group',
        primaryKey: true // Will result in an attribute that is firstName when user facing but first_name in the database
    }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports = ContactGroup;
