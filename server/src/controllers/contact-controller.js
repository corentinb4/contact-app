var Sequelize = require('sequelize');
var express = require('express');
var router = express.Router();
var contactService = require('../services/contact-service');
var sequelize = new Sequelize('phone_book', 'root', 'root', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});

/* GET contacts listing. */
router.get('/', async function(req, res, next) {
  var contacts = await contactService.getContacts();
  res.json(contacts);
});

router.post('/', async function(req, res, next) {
  var contact = await contactService.addContact(req.body);
  res.status(201).json({ status: 201, data: contact, message: "Succesfully Contact Created" });
});

router.delete('/:id', async function(req, res, next) {
  var id = req.params.id;
  await contactService.deleteContact(id);
  res.status(200).json({ status: 200, message: "Succesfully Contact Deleted" });
});

router.put('/:id', async function(req, res, next) {
  var contact = await contactService.updateContact(req.params.id, req.body);
  res.status(200).json({ status: 200, data: contact, message: "Succesfully Contact Updated" });
});

module.exports = router;
