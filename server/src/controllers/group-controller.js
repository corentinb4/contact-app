var Sequelize = require('sequelize');
var express = require('express');
var router = express.Router();
var groupService = require('../services/group-service');
var sequelize = new Sequelize('phone_book', 'root', 'root', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});

router.get('/', async function(req, res, next) {
  var groups = await groupService.getGroups();
  res.json(groups);
});

router.post('/', async function(req, res, next) {
  var group = await groupService.addGroup(req.body);
  res.status(201).json({ status: 201, data: group, message: "Succesfully Group Created" });
});

router.delete('/:id', async function(req, res, next) {
  var id = req.params.id;
  await groupService.deleteGroup(id);
  res.status(200).json({ status: 200, message: "Succesfully Group Deleted" });
});

router.put('/:id', async function(req, res, next) {
  var group = await groupService.updateGroup(req.params.id, req.body);
  res.status(200).json({ status: 200, data: group, message: "Succesfully Group Updated" });
});

module.exports = router;
