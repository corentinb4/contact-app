var groupsJson = require('../db/groups');
var Group = require('../models/group');
var Contact = require('../models/contact');
var ContactGroup = require('../models/contactgroup');


exports.getGroups = async function () {
    const groups = await Group.findAll();

    for (const group of groups) {
        group.metadatas = {};
        group.metadatas.contacts = [];
        const contactgroups = await ContactGroup.findAll({
            where: {
                group: group.id
            }
        });
        for (const value of contactgroups) {
            const selectedContact = await Contact.findAll({
                where: {
                    id: value.contact
                }
            });
            if (selectedContact.length > 0)
                group.metadatas.contacts.push(selectedContact[0]);
        }
    }
    return groups;
};

exports.addGroup = async function (group) {
    const newGroup = await Group.create(group);
    if (group && group.metadatas && group.metadatas.contacts && group.metadatas.contacts.length > 0) {
        const contacts = await Contact.findAll();
        contacts.forEach(c => {
            c.metadatas = {};
            c.metadatas.fullName = `${c.firstName} ${c.lastName}`;
        });
        for (const value of group.metadatas.contacts) {
            const index = contacts.findIndex(c => c.metadatas.fullName === value);
            if (index > -1) {
                await ContactGroup.create(
                    {
                        group: newGroup.id,
                        contact: contacts[index].id
                    }
                )
            }
        }
    }
    return newGroup;
};

exports.deleteGroup = async function (id) {
    await ContactGroup.destroy({
        where: {
            group: id
        }
    });
    return Group.destroy({
        where: {
            id: id
        }
    });
};

exports.updateGroup = async function (id, newGroup) {
    await Group.update(newGroup, {
        where: {
            id: id
        }
    });
    await ContactGroup.destroy({
        where: {
            group: id
        }
    });

    if (newGroup && newGroup.metadatas && newGroup.metadatas.contacts && newGroup.metadatas.contacts.length > 0) {
        const contacts = await Contact.findAll();
        contacts.forEach(c => {
            c.metadatas = {};
            c.metadatas.fullName = `${c.firstName} ${c.lastName}`;
        });
        for (const value of newGroup.metadatas.contacts) {
            const index = contacts.findIndex(c => c.metadatas.fullName === value);
            if (index > -1) {
                console.log(newGroup);
                await ContactGroup.create(
                    {
                        group: id,
                        contact: contacts[index].id
                    }
                )
            }
        }
    }
    return newGroup;
};
