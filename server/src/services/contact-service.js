var contactsJson = require('../db/contacts');
var Contact = require('../models/contact');


exports.getContacts = async function () {
    return await Contact.findAll({
        attributes: ['id', 'firstName', 'lastName', 'email', 'phoneNumber']
    });
};

exports.addContact = async function (contact) {
    return Contact.create(contact);
};

exports.deleteContact = async function (id) {
    return Contact.destroy({
        where: {
            id: id
        }
    });
};

exports.updateContact = async function (id, newContact) {
    return Contact.update(newContact, {
        where: {
            id: id
        }
    });
};
